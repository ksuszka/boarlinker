﻿namespace BoarLinker
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Security.AccessControl;
    using System.Security.Principal;
    using System.Text.RegularExpressions;

    class Program
    {
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.I1)]
        static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, UInt32 dwFlags);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.I1)]
        static extern bool CreateHardLink(string lpFileName, string lpExistingFileName, IntPtr lpSecurityAttributes);

        private delegate string PathConverter(string path);

        private delegate void FileAction(string source, string destination);

        static void ProcessBoarRevision(string repository, int revision, string destination, OperationMode mode, bool setAcl, bool setReadOnlyAttribute)
        {
            PathConverter destinationPathConverter = null;

            var sessionLoadedHandler = new BoarFileProcessor.SessionLoadedHandler(s =>
            {
                var destinationBase = string.Format(destination, revision, s.Name, s.Date);
                destinationPathConverter = new PathConverter(p => Path.Combine(destinationBase, p));
            });

            FileAction fileAction;
            switch (mode)
            {
                case OperationMode.HardLink:
                    Console.WriteLine("Setting hard linking mode.");
                    fileAction = (sourceFile, destinationFile) =>
                    {
                        if (!CreateHardLink(destinationFile, sourceFile, IntPtr.Zero))
                        {
                            throw Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error());
                        }
                    };
                    break;
                case OperationMode.SymbolicLink:
                    Console.WriteLine("Setting symbolic linking mode.");
                    fileAction = (sourceFile, destinationFile) =>
                    {
                        if (!CreateSymbolicLink(destinationFile, sourceFile, 0))
                        {
                            throw Marshal.GetExceptionForHR(Marshal.GetHRForLastWin32Error());
                        }
                    };
                    break;
                case OperationMode.Copy:
                    Console.WriteLine("Setting copy mode.");
                    fileAction = (sourceFile, destinationFile) =>
                    {
                        File.Copy(sourceFile, destinationFile);
                    };
                    break;
                default:
                    throw new ArgumentException(string.Format("Invalid mode \"{0}\".", mode));
            }

            if (setAcl)
            {
                // Set "deny writes" ACL for everybody.
                var originalFileAction = fileAction;
                fileAction = (sourceFile, destinationFile) =>
                {
                    // FileSystemRights.WriteAttributes cannot be set as it would disable ability to create hard links.
                    var rights = FileSystemRights.WriteData | FileSystemRights.AppendData | FileSystemRights.WriteExtendedAttributes;
                    var fileSecurity = File.GetAccessControl(sourceFile);
                    fileSecurity.SetAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), rights, AccessControlType.Deny));
                    File.SetAccessControl(sourceFile, fileSecurity);

                    originalFileAction(sourceFile, destinationFile);
                };
            }

            if (setReadOnlyAttribute)
            {
                var originalFileAction = fileAction;
                fileAction = (sourceFile, destinationFile) =>
                {
                    var attributes = File.GetAttributes(sourceFile);
                    if ((attributes & FileAttributes.ReadOnly) == 0)
                    {
                        File.SetAttributes(sourceFile, attributes | FileAttributes.ReadOnly);
                    }

                    originalFileAction(sourceFile, destinationFile);
                };
            }

            int fileCount = 0;
            var blobAction = new BoarFileProcessor.FileAction((sourceFile, originalFile) =>
            {
                var destinationFile = destinationPathConverter(originalFile);
                Console.WriteLine("\"{0}\" -> \"{1}\"", sourceFile, destinationFile);
                Directory.CreateDirectory(Directory.GetParent(destinationFile).FullName);
                fileAction(sourceFile, destinationFile);
                ++fileCount;
            });

            BoarFileProcessor.Run(repository, revision, blobAction, sessionLoadedHandler);

            Console.WriteLine("Processed {0} files.", fileCount);
            Console.WriteLine("Finished.");
        }

        static void PrintHelp()
        {
            Console.WriteLine(
@"BoarLinker {0}

Create links to or export files from a Boar repository.

Usage:
  boarlinker /repository=<path> /revision=<number> /destination=<path>
             [/mode=<mode>] [/noacl] [/noro]

Parameters:
  /repository=<path>
    Path to the Boar repository.
  /revision=<number>
    An identifier of the revision to be processed.
  /mode=<mode>
    Mode of operation:
      H - create hardlinks (default),
      S - create symbolic links,
      C - copy files.
  /destination=<path>
    Path to the destination folder. Path can contain following tokens:
      {{0}} - revision number,
      {{1}} - session name,
      {{2}} - revision date,
    Standard .Net formatting style can be used for tokens.
    For example, following path
      c:\export\{{1}} [{{0:D4}}] [{{2:yyyy-MM-dd hh.mm.ss}}]\data
    may become
      c:\export\Photos [0005] [2010-12-30 17.54.33]\data
  /noacl
    Do not alter ACL for processed source files. BoarLinker sets ""deny write""
    permission for everyone by default.
  /noro
    Do not set read-only attribute on processed source files.

Example:
  boarlinker /repository=d:\boar_repo /revision=4 /destination=""d:\boar_repo_preview\{{1}} [{{0:D4}}] [{{2:yyyy-MM-dd hh.mm.ss}}]""

Warning:
  Folder structure created in hard/symbolic link mode contains direct links
  to files inside Boar repository. Any changes to content of those files will
  corrupt Boar repository. BoarLinker tries to prevent this by making files
  read-only, but it is still possible for a stubborn user to do irreversible
  damage.

  You can use BoarLinker solely for peeking given revision but you ought to
  use Boar for any other action.
",
                    Assembly.GetExecutingAssembly().GetName().Version
                );
        }

        static int Main(string[] args)
        {
            try
            {
                bool setAcl = true;
                bool setReadOnlyAttribute = true;
                string repository = null;
                int? revision = null;
                string destination = null;
                OperationMode mode = OperationMode.HardLink;

                // Parse command line.
                foreach (var arg in args)
                {
                    var match = Regex.Match(arg, @"^[/-](?<parameter>\w+)([=\:](?<value>.*))?$");
                    if (!match.Success)
                    {
                        throw new ArgumentException(string.Format("Invalid argument: \"{0}\".", arg));
                    }
                    var parameter = match.Groups["parameter"].Value;
                    var value = match.Groups["value"].Value;
                    switch (parameter)
                    {
                        case "repository":
                            repository = value;
                            break;
                        case "destination":
                            destination = value;
                            break;
                        case "mode":
                            switch (value.ToUpper())
                            {
                                case "H":
                                    mode = OperationMode.HardLink;
                                    break;
                                case "S":
                                    mode = OperationMode.SymbolicLink;
                                    break;
                                case "C":
                                    mode = OperationMode.Copy;
                                    break;
                                default:
                                    throw new ArgumentException(string.Format("Unknown operation mode \"{0}\".", value));
                            }
                            break;
                        case "revision":
                            {
                                int rev;
                                if (!int.TryParse(value, out rev))
                                {
                                    throw new ArgumentException(string.Format("Invalid revision identifier \"{0}\".", value));
                                }

                                revision = rev;
                            }
                            break;
                        case "noacl":
                            {
                                setAcl = false;
                            }
                            break;
                        case "noro":
                            {
                                setReadOnlyAttribute = false;
                            }
                            break;
                        default:
                            throw new ArgumentException(string.Format("Unknown parameter \"{0}\".", parameter));
                    }
                }

                // Check if all required arguments are given.
                if (repository == null || !revision.HasValue || destination == null)
                {
                    PrintHelp();
                    return 1;
                }

                // Start processing.
                ProcessBoarRevision(repository, revision.Value, destination, mode, setAcl, setReadOnlyAttribute);
                return 0;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Error: {0}", ex.Message);
                Console.ResetColor();
                return -1;
            }
        }
    }
}
