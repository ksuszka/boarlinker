﻿namespace BoarLinker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Newtonsoft.Json;

    /// <summary>
    /// Class contains method which goes through every file in given Boar revision and calls given callback method.
    /// </summary>
    internal static class BoarFileProcessor
    {
        private const string SessionsFolder = "sessions";
        private const string BlobsFolder = "blobs";
        private const string SessionFile = "session.json";
        private const string BlobListFile = "bloblist.json";

        public class SessionDefinition
        {
            public string Name { get; set; }
            public DateTime Date { get; set; }
        }

        private class BlobDefinition
        {
            public string Path { get; set; }
            public string MD5Sum { get; set; }
        }

        private class CompleteSession
        {
            public SessionDefinition SessionDefinition { get; set; }
            public IList<BlobDefinition> Blobs { get; set; }
        }

        public delegate void FileAction(string blobPath, string originalFilePath);

        public delegate void SessionLoadedHandler(SessionDefinition session);

        /// <summary>
        /// Method iterates through every file in given revision from Boar repository and executes callback method on each file.
        /// </summary>
        /// <param name="repository">Path to Boar repository.</param>
        /// <param name="revision">Identifier of revision in Boar repository.</param>
        /// <param name="fileAction">Callback executed for every file in given revision from repository.</param>
        /// <param name="sessionLoadedHandler">Callback executed when session info is loaded.</param>
        public static void Run(string repository, int revision, FileAction fileAction, SessionLoadedHandler sessionLoadedHandler)
        {
            var sourceRoot = Path.GetFullPath(repository);
            if (!IsFolderBoarRepository(sourceRoot))
            {
                throw new ArgumentException(string.Format("Path \"{0}\" is not a Boar repository!", sourceRoot));
            }

            var sessionsRoot = Path.Combine(sourceRoot, SessionsFolder);
            var session = LoadCompleteSession(sessionsRoot, revision);

            sessionLoadedHandler(session.SessionDefinition);

            var blobBaseDir = Path.Combine(sourceRoot, BlobsFolder);
            foreach (var blob in session.Blobs)
            {
                var sourceFile = Path.Combine(Path.Combine(blobBaseDir, blob.MD5Sum.Substring(0, 2)), blob.MD5Sum);
                fileAction(sourceFile, blob.Path);
            }
        }

        private static CompleteSession LoadCompleteSession(string sessionsRoot, int revision)
        {
            var revisions = new Stack<int>();
            var revisionSet = new Dictionary<int, bool>();      // Safety check against infinite loops.
            SessionDefinition sessionDefinition = null;

            // Load chain of session files.
            while (true)
            {
                revisions.Push(revision);
                revisionSet[revision] = true;

                var sessionFile = Path.Combine(Path.Combine(sessionsRoot, revision.ToString()), SessionFile);;
                if (!File.Exists(sessionFile))
                {
                    throw new FileNotFoundException(string.Format("Cannot find session file for revision {0}.", revision));
                }

                var session = JsonConvert.DeserializeAnonymousType(File.ReadAllText(sessionFile),
                    new
                    {
                        base_session = new Nullable<int>(),
                        client_data = new
                        {
                            timestamp = 0L,
                            name = ""
                        }
                    });

                if (sessionDefinition == null)
                {
                    sessionDefinition = new SessionDefinition()
                    {
                        Name = session.client_data.name,
                        Date = ConvertUnixTimestampToDateTime(session.client_data.timestamp)
                    };
                }

                if (session.base_session.HasValue)
                {
                    var newRevision = session.base_session.Value;

                    // Safety check against infinite loops.
                    if (revisionSet.ContainsKey(newRevision))
                    {
                        throw new InvalidDataException(string.Format("Found circular session dependency at revision {0}.", revision));
                    }

                    revision = newRevision;
                }
                else
                {
                    break;
                }
            }

            var blobs = new Dictionary<string, string>();

            // Process chain of revisions from oldest to newest and build blob list.
            while (revisions.Count > 0)
            {
                revision = revisions.Pop();
                var blobListFile = Path.Combine(Path.Combine(sessionsRoot, revision.ToString()), BlobListFile); ;
                if (!File.Exists(blobListFile))
                {
                    throw new FileNotFoundException(string.Format("Cannot find bloblist file for revision {0}.", revision));
                }

                var blobList = JsonConvert.DeserializeAnonymousType(File.ReadAllText(blobListFile),
                    new[] {
                    new
                    {
                        filename = "",
                        md5sum = "",
                        action = ""
                    }                
                }
                );

                foreach (var blob in blobList)
                {
                    if (blob.action == "remove")
                    {
                        blobs.Remove(blob.filename);
                    }
                    else
                    {
                        blobs[blob.filename] = blob.md5sum;
                    }
                }
            }

            var blobsFlat = new List<BlobDefinition>(blobs.Count);
            foreach (var kvp in blobs)
            {
                blobsFlat.Add(new BlobDefinition()
                {
                    Path = kvp.Key.Replace('/', '\\'),
                    MD5Sum = kvp.Value
                });
            }

            return new CompleteSession() { SessionDefinition = sessionDefinition, Blobs = blobsFlat };
        }

        private static bool IsFolderBoarRepository(string path)
        {
            return Directory.Exists(Path.Combine(path, SessionsFolder)) && Directory.Exists(Path.Combine(path, BlobsFolder));
        }

        private static DateTime ConvertUnixTimestampToDateTime(long timestamp)
        {
            return DateTime.FromFileTime(timestamp * 10000000 + 116444736000000000L);
        }
    }
}
