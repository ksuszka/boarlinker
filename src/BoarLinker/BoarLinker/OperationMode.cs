﻿namespace BoarLinker
{
    enum OperationMode
    {
        HardLink,
        SymbolicLink,
        Copy
    }
}
