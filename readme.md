Introduction
------------

[Boar] is a simple backup/version control tool written in Python. It is very handy for storing binary data like photos. It performs well when folder structure changes a lot but files' content does not change often. Main adavantage of Boar is a fact that it stores data directly as plain files (called *blobs*) in a file system, so they can be accessed with any application. However to access requested file its original name needs to be converted into the hashed version used internally by Boar.

Boar comes with **boarmount** command which can be used to mount Boar repository as a temporary, read-only file system. Unfortunately this command works only on Linux and does not work on Windows. *BoarLinker* is suppose to fill this gap.

*BoarLinker* does not create a virtual file system as the original boarmount command. Instead it uses NTFS hard linking feature to recreate logical folder structure with files replaced with hard links to *blobs*. Because hard links are implemented on a file system level, they are treated as normal files by all applications.

**Warning: Because hard links point directly to *blobs*, any changes to content of files visible through hard links will corrupt Boar repository. BoarLinker tries to prevent this by making *blobs* read-only, but it is still possible for a stubborn user to do irreversible damage. You can use BoarLinker solely for peeking given revision but you ought to use Boar for any other action.**

However deleting hard linked version of given file is OK, because only link is deleted, not the blob file.

Requirements and restrictions
------------

- .Net Framework 2.0 is required to run *BoarLinker*
- Boar repository must be located on NTFS file system for hardlinking mode to work correctly.
- Destination folder must be located on the same partition as Boar repository for hardlinking mode to work correctly.


References
----------

*BoarLinker* uses:

- [Json.NET] (to read Boar repository metadata)
- [Costura] (to merge references into single executable)
- [mpress] (to reduce size of final executable)
- [MSBuildVersioningTask] (to maintain source code version numbers)

Usage
-----

    Usage:
      boarlinker /repository=<path> /revision=<number> /destination=<path> [/mode=<mode>] [/noacl] [/noro]

    Parameters:
      /repository=<path>
        Path to the Boar repository.
      /revision=<number>
        An identifier of the revision to be processed.
      /mode=<mode>
        Mode of operation:
          H - create hardlinks (default),
          S - create symbolic links,
          C - copy files.
      /destination=<path>
        Path to the destination folder. Path can contain following tokens:
          {0} - revision number,
          {1} - session name,
          {2} - revision date,
        Standard .Net formatting style can be used for tokens.
        For example, following path
          c:\export\{1} [{0:D4}] [{2:yyyy-MM-dd hh.mm.ss}]\data
        may become
          c:\export\Photos [0005] [2010-12-30 17.54.33]\data
      /noacl
        Do not alter ACL for processed source files. BoarLinker sets ""deny write"" permission for everyone by default.
      /noro
        Do not set read-only attribute on processed source files.

    Example:
      boarlinker /repository=d:\boar_repo /revision=4 /destination="d:\boar_repo_preview\{1} [{0:D4}] [{2:yyyy-MM-dd hh.mm.ss}]"


[boar]: http://code.google.com/p/boar/
[mpress]: http://www.matcode.com/mpress.htm
[costura]: http://code.google.com/p/costura/
[Json.NET]: http://json.codeplex.com/
[MSBuildVersioningTask]: http://versioning.codeplex.com/
